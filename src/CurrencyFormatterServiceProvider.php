<?php

namespace Teknomuslim\CurrencyFormatter;

use Illuminate\Support\ServiceProvider;

class currencyFormatterServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->loadRoutesFrom(__DIR__.'/routes.php');
		$this->loadViewsFrom(__DIR__.'/views', 'CurrencyFormatter');
		$this->publishes([
			__DIR__.'/routes.php',
			__DIR__.'/views', 'CurrencyFormatter',
			__DIR__.'/../resources/views' => resource_path('views/vendor/currency')

		]);

	}
	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->make('Teknomuslim\CurrencyFormatter\Controllers\CurrencyFormatterController');
	}
}